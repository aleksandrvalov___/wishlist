package main

import (
	"net"
	"os"
	"time"

	"bitbucket.org/aleksandrvalov___/wishlist/pkg/wishservice"

	"bitbucket.org/aleksandrvalov___/wishlist/internal/services/wish"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_logrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	_ "github.com/go-sql-driver/mysql"

	"bitbucket.org/aleksandrvalov___/wishlist/internal/handler"
	"bitbucket.org/aleksandrvalov___/wishlist/internal/services/user"
)

func main() {
	logger := &logrus.Logger{
		Out:       os.Stdout,
		Formatter: new(logrus.JSONFormatter),
		Hooks:     make(logrus.LevelHooks),
		Level:     logrus.InfoLevel,
	}
	loggerEntry := logrus.NewEntry(logger)
	grpc_logrus.ReplaceGrpcLogger(loggerEntry)

	grpcServer := grpc.NewServer(grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
		grpc_logrus.UnaryServerInterceptor(loggerEntry),
		grpc_recovery.UnaryServerInterceptor(),
	)))

	db, err := getDB()
	if err != nil {
		logger.Fatalf("db connect fialed^ %v", err)
	}
	defer db.Close()

	userService := user.New(db)
	wishService := wish.New(db)

	h := handler.New(userService, wishService)

	wishservice.RegisterWishlistServer(grpcServer, h)

	lis, err := net.Listen("tcp", ":4772")
	if err != nil {
		logger.Fatalf("failed to listen: %v", err)
	}

	err = grpcServer.Serve(lis)
	if err != nil {
		logger.Fatalf("start server error")
	}
}

func getDB() (*sqlx.DB, error) {
	db, err := sqlx.Open("mysql", "user:pass@/wishlist?parseTime=true")
	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(4)
	db.SetMaxIdleConns(4)

	return db, nil
}
