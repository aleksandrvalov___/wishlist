BEGIN;

CREATE TABLE user
(
    id         BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    user_name  VARCHAR(255) DEFAULT '',
    first_name VARCHAR(255) DEFAULT '',
    last_name  VARCHAR(255) DEFAULT '',
    tg_id      BIGINT UNSIGNED not null,
    phone      VARCHAR(20)  DEFAULT '',
    created_at DATETIME        NOT NULL
);

create unique index idx_name on user (user_name);
create unique index idx_tg_id on user (tg_id);

CREATE TABLE wish
(
    id         BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    user_id    BIGINT UNSIGNED NOT NULL,
    title      VARCHAR(255) DEFAULT '',
    content    TEXT,
    created_at DATETIME        NOT NULL
);

create index idx_user_id on wish (user_id);

CREATE TABLE wish_group
(
    id         BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    user_id    BIGINT UNSIGNED NOT NULL,
    title      VARCHAR(255) DEFAULT '',
    created_at DATETIME        NOT NULL
);

create index idx_user_id on wish_group (user_id);

CREATE TABLE wish_in_group
(
    wish_id    BIGINT UNSIGNED NOT NULL,
    group_id   BIGINT UNSIGNED NOT NULL,
    created_at DATETIME        NOT NULL,
    PRIMARY KEY (wish_id, group_id)
);

COMMIT;