module bitbucket.org/aleksandrvalov___/wishlist/tools

go 1.16

require (
	github.com/golangci/golangci-lint v1.39.0
	golang.org/x/tools v0.1.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/ini.v1 v1.51.1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
