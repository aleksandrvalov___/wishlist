package model

import "time"

type User struct {
	ID        int64     `db:"id"`
	TgID      int64     `db:"tg-id"`
	UserName  string    `db:"user_name"`
	FirstName string    `db:"first_name"`
	LastName  string    `db:"last_name"`
	Phone     string    `db:"phone"`
	CreatedAt time.Time `db:"created_at"`
}

type Wish struct {
	ID        int64     `db:"id"`
	UserID    int64     `db:"user_id"`
	Title     string    `db:"title"`
	Content   string    `db:"content"`
	CreatedAt time.Time `db:"created_at"`
}
