package wish

import (
	"context"
	"time"

	sq "github.com/Masterminds/squirrel"

	"bitbucket.org/aleksandrvalov___/wishlist/internal/model"
)

func (s Service) Add(ctx context.Context, userID int64, content string) (*model.Wish, error) {
	query, args, err := sq.StatementBuilder.
		Insert(tableName).
		Columns("user_id", "content", "created_at").
		Values(userID, content, time.Now()).
		ToSql()
	if err != nil {
		return nil, err
	}

	stmt, err := s.db.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = stmt.Close()
	}()

	res, err := stmt.ExecContext(ctx, args...)
	if err != nil {
		return nil, err
	}

	wishID, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}

	return &model.Wish{
		ID:      wishID,
		Content: content,
	}, nil
}
