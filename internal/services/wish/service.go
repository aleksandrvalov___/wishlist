package wish

import "github.com/jmoiron/sqlx"

const (
	tableName = "wish"

	errFormat = "wish service: %v"
)

type Service struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *Service {
	return &Service{db: db}
}
