package user

import (
	"context"
	"database/sql"
	"fmt"

	sq "github.com/Masterminds/squirrel"

	"bitbucket.org/aleksandrvalov___/wishlist/internal/model"
)

func (s Service) GetByID(ctx context.Context, id int64) (*model.User, error) {
	builder := sq.StatementBuilder.
		Select(fields...).
		From(tableName).
		Where(sq.Eq{"id": id})

	query, args, err := builder.ToSql()
	if err != nil {
		return nil, fmt.Errorf(errFormat, err)
	}

	var result []model.User
	err = s.db.SelectContext(ctx, &result, query, args...)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}

		return nil, fmt.Errorf(errFormat, err)
	}

	if len(result) > 0 {
		return &result[0], nil
	}

	return nil, nil
}
