package user

import (
	"context"
	"time"

	sq "github.com/Masterminds/squirrel"

	"bitbucket.org/aleksandrvalov___/wishlist/internal/model"
)

func (s Service) Create(ctx context.Context, user model.User) (*model.User, error) {
	createdAt := time.Now()
	query, args, err := sq.StatementBuilder.
		Insert(tableName).
		SetMap(map[string]interface{}{
			"tg_id":      user.TgID,
			"user_name":  user.UserName,
			"first_name": user.FirstName,
			"last_name":  user.LastName,
			"created_at": createdAt,
		}).
		ToSql()
	if err != nil {
		return nil, err
	}

	stmt, err := s.db.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = stmt.Close()
	}()

	res, err := stmt.ExecContext(ctx, args...)
	if err != nil {
		return nil, err
	}

	userID, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}

	user.ID = userID
	user.CreatedAt = createdAt

	return &user, nil
}
