package user

import "github.com/jmoiron/sqlx"

const (
	tableName = "user"

	errFormat = "user service: %v"
)

var (
	fields = []string{"id", "tg_id", "user_name", "first_name", "last_name", "created_at"}
)

type Service struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *Service {
	return &Service{db: db}
}
