package handler

import (
	"context"

	"bitbucket.org/aleksandrvalov___/wishlist/pkg/wishservice"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"bitbucket.org/aleksandrvalov___/wishlist/internal/model"
)

var (
	errUserAlreadyExists = status.Errorf(codes.AlreadyExists, "user already exist")

	errUndefinedUser = status.Errorf(codes.InvalidArgument, "undefined user")
)

type Server struct {
	wishservice.UnsafeWishlistServer

	user User
	wish Wish
}

func New(user User, wish Wish) *Server {
	return &Server{
		user: user,
		wish: wish,
	}
}

type User interface {
	Create(ctx context.Context, user model.User) (*model.User, error)
	GetByTgID(ctx context.Context, tgID int64) (*model.User, error)
	GetByID(ctx context.Context, id int64) (*model.User, error)
}

type Wish interface {
	Add(ctx context.Context, userID int64, content string) (*model.Wish, error)
}
