package handler

import (
	"context"

	"bitbucket.org/aleksandrvalov___/wishlist/internal/model"
	"bitbucket.org/aleksandrvalov___/wishlist/pkg/wishservice"
)

func (s Server) Register(ctx context.Context, request *wishservice.RegisterRequest) (*wishservice.RegisterResponse, error) {
	existUser, err := s.user.GetByTgID(ctx, request.GetTgId())
	if err != nil {
		return nil, err
	}

	if existUser != nil {
		return nil, errUserAlreadyExists
	}

	created, err := s.user.Create(ctx, model.User{
		TgID:      request.GetTgId(),
		UserName:  request.GetUserName(),
		FirstName: request.GetFirstName(),
		LastName:  request.GetLastName(),
	})
	if err != nil {
		return nil, err
	}

	return &wishservice.RegisterResponse{
		UserId: created.ID,
	}, nil
}
