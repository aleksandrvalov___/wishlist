package handler

import (
	"context"

	"bitbucket.org/aleksandrvalov___/wishlist/pkg/wishservice"
)

func (s Server) AddWish(ctx context.Context, request *wishservice.AddWishRequest) (*wishservice.AddWishResponse, error) {
	user, err := s.user.GetByID(ctx, request.GetUserId())
	if err != nil {
		return nil, err
	}

	if user == nil {
		return nil, errUndefinedUser
	}

	created, err := s.wish.Add(ctx, request.GetUserId(), request.GetContent())
	if err != nil {
		return nil, err
	}

	return &wishservice.AddWishResponse{
		WishId: created.ID,
	}, nil
}
