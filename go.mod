module bitbucket.org/aleksandrvalov___/wishlist

go 1.17

require (
	bitbucket.org/aleksandrvalov___/wishlist/pkg/wishservice v0.0.0-00010101000000-000000000000
	github.com/Masterminds/squirrel v1.5.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/sirupsen/logrus v1.8.1
	google.golang.org/grpc v1.42.0
)

require (
	github.com/golang/protobuf v1.5.0 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/sys v0.0.0-20210603081109-ebe580a85c40 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)

replace bitbucket.org/aleksandrvalov___/wishlist/pkg/wishservice => ./pkg/wishservice
