BUILD_ENVPARAMS:=CGO_ENABLED=0
TEST_ENVPARAMS:=CGO_ENABLED=1

GO_PACKAGE=$(shell go list -m)


export PATH:=$(PWD)/bin:$(PATH)
export TEST_CONFIG_PATH:=$(PWD)

.PHONY: gen
gen:
	$(info # Generate go file from proto ...)
	@protoc --go_out=. --go-grpc_out=. ./api/wishlist.proto
	@make format


.PHONY: lint
lint:
	$(info # Running lint ...)
	@./bin/golangci-lint run --config=.golangci.yml ./...

.PHONY: tools
tools:
	$(info # Generate tools in ./bin ...)
	@cd tools && go mod download && go generate -tags tools

.PHONY: format
format:
	$(info # Formatting ...)
	@./bin/goimports -local $(GO_PACKAGE) -w .


.PHONY: build
build:
	$(info # Building ...)
	@$(BUILD_ENVPARAMS) go build -ldflags "$(LDFLAGS)" -o ./bin/service ./cmd/service

.PHONY: up
up:
	$(info # Start local infrastructure services)
	@cp -n docker-compose.yaml.dist docker-compose.yaml
	@docker-compose up -d

.PHONY: down
down:
	$(info # Stop local infrastructure services)
	@docker-compose down

